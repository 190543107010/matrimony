package com.example.matrimony.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {

    public  static String getformatedstingtoinsert(String datetoconvert)
    {
        SimpleDateFormat format1 = new SimpleDateFormat("dd/mm/yyyy");
        SimpleDateFormat format2 = new SimpleDateFormat("yyyy-mm-dd");
        String convertdate= "";
        try
        {
            Date date = format1.parse(datetoconvert);
           convertdate = format2.format(date);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return convertdate;
    }
}
