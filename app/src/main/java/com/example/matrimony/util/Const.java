package com.example.matrimony.util;

public class Const {

    public static final int male = 1;
    public static final int female = 2;
    public static final int other = 3;
    public static final String gender ="Gender";

    public static final String USER_OBJECT ="UserObject";

    public static final String cricket = "cricket";
    public static final String hokkey = "hokkey";
    public static final String footboll = "footboll";
}
