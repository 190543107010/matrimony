package com.example.matrimony.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.matrimony.Model.CityModel;

import java.util.ArrayList;
import java.util.HashMap;

public class TblmstCity extends MyDatabase{

    public static final String TABLE_NAME = "TblMstCity";
    public static final String CITY_ID = "CityId";
    public static final String CITY_NAME = "Name";

    public TblmstCity(Context context) {
        super(context);
    }
    public ArrayList<CityModel> getCityList()
    {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<CityModel> list = new ArrayList<>();
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query,null);
        cursor.moveToFirst();

        for(int i=0;i < cursor.getCount();i++)
        {
           CityModel cm = new CityModel();
           cm.setCityId(cursor.getInt(cursor.getColumnIndex(CITY_ID)));
           cm.setName(cursor.getString(cursor.getColumnIndex(CITY_NAME)));
            list.add(cm);
            cursor.moveToNext();
        }
        CityModel cityModel =new CityModel();
        cityModel.setName("Select City");
        list.add(0,cityModel);
        cursor.close();
        db.close();
        return list;
    }
    public CityModel getcitybyid(int id)
    {
        SQLiteDatabase db=getReadableDatabase();
        CityModel model= new CityModel();
        String query = "SELECT * FROM "+ TABLE_NAME +" WHERE "+ CITY_ID + " = ?";
        Cursor cursor = db.rawQuery(query,new String[]{String.valueOf(id)});
        cursor.moveToFirst();
        model.setName(cursor.getString(cursor.getColumnIndex(CITY_NAME)));
        model.setCityId(cursor.getInt(cursor.getColumnIndex(CITY_ID)));
        cursor.close();
        db.close();
        return model;
    }
}
