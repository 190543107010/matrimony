package com.example.matrimony.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

public class MyDatabase extends SQLiteAssetHelper {

    public static final String DATABASE_NAME = "matrimony1.db";
    public static final Integer DATAVASE_VERSION = 2;


    public MyDatabase(Context context) {
        super(context, DATABASE_NAME, null,DATAVASE_VERSION );
    }
}
