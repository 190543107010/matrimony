package com.example.matrimony.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.icu.lang.UScript;
import android.provider.ContactsContract;

import com.example.matrimony.Model.UserModel;

import java.util.ArrayList;

public class TblUser extends MyDatabase {

    public static final String TABLE_NAME="TblUser";
    public static final String USER_ID="UserId";
    public static final String USER_NAME="Name";
    public static final String FATHER_NAME="FatherName";
    public static final String SURNAME="SurName";
    public static final String GENDER="Gender";
    public static final String HOBBY="Hobby";
    public static final String DOB="Dob";
    public static final String PHONE_NUMBER="PhoneNumber";
    public static final String LANGUANGE_ID="LanguangeId";
    public static final String CITY_ID="CityId";
    public static final String IS_FAVORITE="IsFavorite";
     /* qury for database*/
    public static final String CITY="City";
    public static final String LANGUANGE="Languange";
    public static final String EMAIL="Email";

    public TblUser(Context context) {
        super(context);
    }

    public ArrayList<UserModel> getUsersList()
    {
        String query =(
                " SELECT " +
                        " UserId," +
                        " TblUser.Name," +
                        " FatherName," +
                        " SurName," +
                        " GENDER," +
                        " Hobby," +
                        " Dob," +
                        " PhoneNumber," +
                        " TblMstLanguange.LanguangeId," +
                        " TblMstCity.CityId," +
                        " Email,"+
                        " IsFavorite,"+
                        " TblMstLanguange.Name as Languange," +
                        " TblmstCity.Name as City " +

                        " FROM " +
                        " TblUser " +
                        " INNER JOIN TblMstLanguange ON TblUser.LanguangeId = TblMstLanguange.LanguangeId " +
                        " INNER JOIN TblMstCity ON TblUser.CityId = TblMstCity.CityId ") ;
        SQLiteDatabase db=getReadableDatabase();
        ArrayList<UserModel> list = new ArrayList<>();
        Cursor cursor=db.rawQuery(query,null);
        cursor.moveToFirst();
        for (int i=0;i < cursor.getCount();i++)
        {
            list.add(getuserdetails(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }

    public UserModel getuserdetails(Cursor cursor)
    {
        UserModel userModel = new UserModel();
        userModel.setUserId(cursor.getInt(cursor.getColumnIndex(USER_ID)));
        userModel.setCityId(cursor.getInt(cursor.getColumnIndex(CITY_ID)));
        userModel.setLanguangeId(cursor.getInt(cursor.getColumnIndex(LANGUANGE_ID)));
        userModel.setGender(cursor.getInt(cursor.getColumnIndex(GENDER)));
        userModel.setName(cursor.getString(cursor.getColumnIndex(USER_NAME)));
        userModel.setFatherName(cursor.getString(cursor.getColumnIndex(FATHER_NAME)));
        userModel.setSurName(cursor.getString(cursor.getColumnIndex(SURNAME)));
        userModel.setDob(cursor.getString(cursor.getColumnIndex(DOB)));
        userModel.setHobby(cursor.getString(cursor.getColumnIndex(HOBBY)));
        userModel.setPhoneNumber(cursor.getString(cursor.getColumnIndex(PHONE_NUMBER)));
        userModel.setCity(cursor.getString(cursor.getColumnIndex(CITY)));
        userModel.setLanguange(cursor.getString(cursor.getColumnIndex(LANGUANGE)));
        userModel.setEmail(cursor.getString(cursor.getColumnIndex(EMAIL)));
        userModel.setIsFavorite(cursor.getInt(cursor.getColumnIndex(IS_FAVORITE)));
        return userModel;
    }

    public UserModel getuserbyid(int id)
    {
        SQLiteDatabase db = getReadableDatabase();
        UserModel userModel =new UserModel();
        String query = "SELECT * FROM "+ TABLE_NAME + " WHERE " + USER_ID + " = ?";
        Cursor cursor =db.rawQuery(query,new String[]{String.valueOf(id)});
        cursor.moveToFirst();
       userModel= getuserdetails(cursor);
        cursor.close();
        db.close();
        return userModel;
    }

    public ArrayList<UserModel> getuserbygender(int gender)
    {
        SQLiteDatabase db =getReadableDatabase();
        ArrayList<UserModel> list = new ArrayList<>();
        String query =
                " SELECT " +
                        " UserId," +
                        " TblUser.Name," +
                        " FatherName," +
                        " SurName," +
                        " Gender," +
                        " Hobby," +
                        " Dob," +
                        " PhoneNumber," +
                        " Email,"+
                        " TblMstLanguange.LanguangeId," +
                        " TblMstCity.CityId," +
                        " Email,"+
                        " IsFavorite,"+
                        " TblMstLanguange.Name as Languange," +
                        " TblmstCity.Name as City " +

                        " FROM " +
                        " TblUser " +
                        " INNER JOIN TblMstLanguange ON TblUser.LanguangeId = TblMstLanguange.LanguangeId " +
                        " INNER JOIN TblMstCity ON TblUser.CityId = TblMstCity.CityId "+
                        " WHERE "+
                        " Gender = ?";
        Cursor cursor = db.rawQuery(query,new String[]{String.valueOf(gender)});
        cursor.moveToFirst();
        for (int i=0;i < cursor.getCount();i++)
        {
            list.add(getuserdetails(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }

    public ArrayList<UserModel> getfavoriteuserlist ()
    {
        SQLiteDatabase db =getReadableDatabase();
        ArrayList<UserModel> list = new ArrayList<>();
        String query =
                " SELECT " +
                        " UserId," +
                        " TblUser.Name," +
                        " FatherName," +
                        " SurName," +
                        " Gender," +
                        " Hobby," +
                        " Dob," +
                        " PhoneNumber," +
                        " Email,"+
                        " TblMstLanguange.LanguangeId," +
                        " TblMstCity.CityId," +
                        " Email,"+
                        " IsFavorite,"+
                        " TblMstLanguange.Name as Languange," +
                        " TblmstCity.Name as City " +

                        " FROM " +
                        " TblUser " +
                        " INNER JOIN TblMstLanguange ON TblUser.LanguangeId = TblMstLanguange.LanguangeId " +
                        " INNER JOIN TblMstCity ON TblUser.CityId = TblMstCity.CityId "+
                        " WHERE "+
                        " IsFavorite = ?";
        Cursor cursor = db.rawQuery(query,new String[]{String.valueOf(1)});
        cursor.moveToFirst();
        for (int i=0;i < cursor.getCount();i++)
        {
            list.add(getuserdetails(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }

    public Long insertuser(String name,String fathername,String surname,int gender,
                           String hobby,String date,String phoneno,int languageid,int cityid,String Email,int isFavorite)
    {
        SQLiteDatabase db=getWritableDatabase();
        ContentValues cv =new ContentValues();
        cv.put(USER_NAME,name);
        cv.put(FATHER_NAME,fathername);
        cv.put(SURNAME,surname);
        cv.put(GENDER,gender);
        cv.put(HOBBY,hobby);
        cv.put(DOB,date);
        cv.put(PHONE_NUMBER,phoneno);
        cv.put(LANGUANGE_ID,languageid);
        cv.put(CITY_ID,cityid);
        cv.put(EMAIL,Email);
        cv.put(IS_FAVORITE,isFavorite);
       Long lastinsertedid = db.insert(TABLE_NAME,null,cv);
        db.close();
        return lastinsertedid;
    }

    public int udateuser(String name,String fathername,String surname,int gender,String hobby,String date,
                          String phoneno,int languageid,int cityid,String email,int isFavorite,int userid)
    {
        SQLiteDatabase db=getWritableDatabase();
        ContentValues cv =new ContentValues();
        cv.put(USER_NAME,name);
        cv.put(FATHER_NAME,fathername);
        cv.put(SURNAME,surname);
        cv.put(GENDER,gender);
        cv.put(HOBBY,hobby);
        cv.put(DOB,date);
        cv.put(PHONE_NUMBER,phoneno);
        cv.put(LANGUANGE_ID,languageid);
        cv.put(CITY_ID,cityid);
        cv.put(EMAIL,email);
        cv.put(IS_FAVORITE,isFavorite);
        int lastupadtedid = db.update(TABLE_NAME,cv,USER_ID + " = ?",new String[]{String.valueOf(userid)});
        db.close();
        return lastupadtedid;
    }

    public int deleteuser(int userid)
    {
        SQLiteDatabase db=getWritableDatabase();
        int lastdeletedid = db.delete(TABLE_NAME,USER_ID + " = ?",new String[]{String.valueOf(userid)});
        db.close();
        return lastdeletedid;
    }
    public int updateFavoriteStatus(int isfavorite,int userid)
    {
        SQLiteDatabase db=getWritableDatabase();
        ContentValues cv =new ContentValues();
        cv.put(IS_FAVORITE,isfavorite);
        int lastupadtedid = db.update(TABLE_NAME,cv,USER_ID + " = ?",new String[]{String.valueOf(userid)});
        db.close();
        return lastupadtedid;

    }
}
