package com.example.matrimony.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.matrimony.Model.LanguangeModel;

import java.util.ArrayList;
import java.util.HashMap;

public class Tblmstlanguange extends MyDatabase {

    public static final String TABLE_NAME = "TblMstLanguange";
    public static final String LANGUANGE_ID = "LanguangeId";
    public static final String  LANGUAGENAME = "Name";

    public Tblmstlanguange(Context context) {
        super(context);
    }

    public ArrayList<LanguangeModel> getLanguangesList()
    {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<LanguangeModel> list = new ArrayList<>();
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query,null);
        cursor.moveToFirst();
        for(int i=0;i < cursor.getCount();i++)
        {
            LanguangeModel lm =new LanguangeModel();
            lm.setLanguangeId(cursor.getInt(cursor.getColumnIndex(LANGUANGE_ID)));
            lm.setName(cursor.getString(cursor.getColumnIndex(LANGUAGENAME)));
            list.add(lm);
            cursor.moveToNext();
        }
        LanguangeModel languangeModel = new LanguangeModel();
        languangeModel.setName("Select Languange");
        list.add(0,languangeModel);
        cursor.close();
        db.close();
        return list;
    }
}
