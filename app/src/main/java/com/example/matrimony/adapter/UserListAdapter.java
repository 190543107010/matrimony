package com.example.matrimony.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.matrimony.Model.UserModel;
import com.example.matrimony.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.UserHolder> {
    Context context;
    ArrayList<UserModel> userList;
    onViewCilickListner onViewCilickListner;

    public UserListAdapter(Context context, ArrayList<UserModel> userList, onViewCilickListner onViewCilickListner) {
        this.context = context;
        this.userList = userList;
        this.onViewCilickListner = onViewCilickListner;
    }

    public interface onViewCilickListner
    {
        void onclick(int position);
        void onFavroiteClick(int position);
        void onItemclick(int position);
    }

    @Override
    public UserHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new UserHolder(LayoutInflater.from(context).inflate(R.layout.view_row_user_list, null));
    }

    @Override
    public void onBindViewHolder(UserHolder holder, int position) {
        holder.tvfullname.setText(userList.get(position).getName()+" "+userList.get(position).getSurName());
        holder.tvlanguange.setText(userList.get(position).getLanguange()+" | "+userList.get(position).getCity());
        holder.tvDob.setText(userList.get(position).getDob());
        holder.ivFavoriteUser.setImageResource(userList.get(position).getIsFavorite()== 0?R.drawable.baseline_favorite_border_black_24:R.drawable.baseline_favorite_black_24);

        holder.ivDeleteUser.setOnClickListener(view -> {
            if(onViewCilickListner != null)
            {
                onViewCilickListner.onclick(position);
            }
        });

        holder.ivFavoriteUser.setOnClickListener(view -> {
            if(onViewCilickListner != null)
            {
                onViewCilickListner.onFavroiteClick(position);
            }
        });

        holder.itemView.setOnClickListener(view -> {
            if(onViewCilickListner != null)
            {
                onViewCilickListner.onItemclick(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    class UserHolder extends RecyclerView.ViewHolder {

            @BindView(R.id.tvfullname)
            TextView tvfullname;
            @BindView(R.id.tvlanguange)
            TextView tvlanguange;
            @BindView(R.id.tvDob)
            TextView tvDob;
        @BindView(R.id.ivDeleteUser)
        ImageView ivDeleteUser;
        @BindView(R.id.ivFavoriteUser)
        ImageView ivFavoriteUser;
        public UserHolder(@NonNull View itemview) {
            super(itemview);
            ButterKnife.bind(this, itemview);
        }
    }

}
