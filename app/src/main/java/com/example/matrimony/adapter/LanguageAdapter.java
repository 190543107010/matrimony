package com.example.matrimony.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import androidx.core.content.ContextCompat;

import com.example.matrimony.Model.LanguangeModel;
import com.example.matrimony.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LanguageAdapter extends BaseAdapter
{
    Context context;
    ArrayList<LanguangeModel> languagelist;

    public LanguageAdapter (Context context, ArrayList<LanguangeModel> languagelist) {
        this.context = context;
        this.languagelist = languagelist;
    }

    @Override
    public int getCount() {
        return languagelist.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        LanguageAdapter.ViewHolder viewHolder;
        if(v == null)
        {
            v = LayoutInflater.from(context).inflate(R.layout.view_row_text, null);
            viewHolder = new ViewHolder(v);
            v.setTag(viewHolder);
        }else {
            viewHolder =(ViewHolder) v.getTag();
        }
        viewHolder.tvname.setText(languagelist.get(i).getName());
        if(i == 0)
        {
            viewHolder.tvname.setTextColor(ContextCompat.getColor(context,R.color.color_gray));
        }
        else
        {
            viewHolder.tvname.setTextColor(ContextCompat.getColor(context,R.color.colorblack));
        }
        return v;
    }

    static
    class ViewHolder {
        @BindView(R.id.tvname)
        TextView tvname;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}

