package com.example.matrimony.adapter;


import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.matrimony.fragment.UserListFragment;
import com.example.matrimony.util.Const;

public class ViewPagerAdapter extends FragmentStatePagerAdapter
{

    private String tabTitles[] = new String[]{"Male","Female"};
    private Context context;

    public ViewPagerAdapter(@NonNull FragmentManager fm ,int behavior, Context context) {
        super(fm,behavior);

    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        if(position == 0) {
            return UserListFragment.getInstance(Const.male);
        }
        else {
            return UserListFragment.getInstance(Const.female);
        }

    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}
