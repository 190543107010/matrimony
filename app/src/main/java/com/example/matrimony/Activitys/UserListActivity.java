package com.example.matrimony.Activitys;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.matrimony.Model.UserModel;
import com.example.matrimony.R;
import com.example.matrimony.adapter.UserListAdapter;
import com.example.matrimony.database.TblUser;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserListActivity extends baseactivity {

    @BindView(R.id.rcvUserlist)
    RecyclerView rcvUserlist;

    ArrayList<UserModel> userlist = new ArrayList<>();
    UserListAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
        ButterKnife.bind(this);
        setupActionbar(getString(R.string.favorite_user),true);
        setAdapter();
    }
    void setAdapter()
    {
        rcvUserlist.setLayoutManager(new GridLayoutManager(this,1));
        userlist.addAll(new TblUser(this).getfavoriteuserlist());
        adapter = new UserListAdapter(this, userlist, new UserListAdapter.onViewCilickListner() {
            @Override
            public void onclick(int position) {

            }

            @Override
            public void onFavroiteClick(int position) {
                int lastUpdatedUserId = new TblUser(UserListActivity.this).updateFavoriteStatus( 0,userlist.get(position).getUserId());
                if(lastUpdatedUserId > 0){
                    userlist.remove(position);
                    adapter.notifyItemRemoved(position);
                    adapter.notifyItemRangeChanged(0,userlist.size());
                }
            }

            @Override
            public void onItemclick(int position) {

            }
        });
        rcvUserlist.setAdapter(adapter);
    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(UserListActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
