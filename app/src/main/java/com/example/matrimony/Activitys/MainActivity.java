package com.example.matrimony.Activitys;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import com.example.matrimony.R;
import com.example.matrimony.database.MyDatabase;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class MainActivity extends baseactivity {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.cvactregs)
    CardView cvactregs;
    @BindView(R.id.cvactlst)
    CardView cvactlst;
    @BindView(R.id.cvactfav)
    CardView cvactfav;
    @BindView(R.id.cvactserach)
    CardView cvactserach;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setupActionbar(getString(R.string.lbl_dashbord),false);
        new MyDatabase(this).getWritableDatabase();
    }

    @OnClick(R.id.cvactregs)
    public void onCvactregsClicked() {
        Intent intent = new Intent(MainActivity.this, AdduserActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.cvactlst)
    public void onCvactlstClicked() {
        Intent intent = new Intent(MainActivity.this, ActivityUserListGender.class);
        startActivity(intent);
    }

    @OnClick(R.id.cvactfav)
    public void onCvactfavClicked() {
        Intent intent = new Intent(MainActivity.this, UserListActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.cvactserach)
    public void onCvactserachClicked() {
        Intent intent = new Intent(MainActivity.this,ActivitySearchUser.class);
        startActivity(intent);
    }
}
