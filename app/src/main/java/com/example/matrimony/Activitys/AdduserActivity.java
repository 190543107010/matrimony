package com.example.matrimony.Activitys;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.matrimony.Model.CityModel;
import com.example.matrimony.Model.LanguangeModel;
import com.example.matrimony.Model.UserModel;
import com.example.matrimony.R;
import com.example.matrimony.adapter.CityAdapter;
import com.example.matrimony.adapter.LanguageAdapter;
import com.example.matrimony.database.TblUser;
import com.example.matrimony.database.TblmstCity;
import com.example.matrimony.database.Tblmstlanguange;
import com.example.matrimony.fragment.UserListFragment;
import com.example.matrimony.util.Const;
import com.example.matrimony.util.Utils;
import com.google.android.material.radiobutton.MaterialRadioButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AdduserActivity extends baseactivity {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.etname)
    TextInputEditText etname;
    @BindView(R.id.etfname)
    TextInputEditText etfname;
    @BindView(R.id.etsname)
    TextInputEditText etsname;
    @BindView(R.id.rbmale)
    MaterialRadioButton rbmale;
    @BindView(R.id.rgfemale)
    MaterialRadioButton rgfemale;
    @BindView(R.id.rggender)
    RadioGroup rggender;
    @BindView(R.id.etdob)
    TextInputEditText etdob;
    @BindView(R.id.etphone)
    TextInputEditText etphone;
    @BindView(R.id.etemail)
    TextInputEditText etemail;
    @BindView(R.id.spcity)
    Spinner spcity;
    @BindView(R.id.splanguage)
    Spinner splanguage;
    @BindView(R.id.btnsubmit)
    Button btnsubmit;

    CityAdapter cityAdapter;
    LanguageAdapter languageAdapter;

    ArrayList<CityModel> citylist = new ArrayList<>();
    ArrayList<LanguangeModel> languangelist = new ArrayList<>();
    @BindView(R.id.chkcrik)
    CheckBox chkcrik;
    @BindView(R.id.chkhoky)
    CheckBox chkhoky;
    @BindView(R.id.chkfootbol)
    CheckBox chkfootbol;
    @BindView(R.id.Screen_Layout)
    FrameLayout ScreenLayout;

    UserModel userModel;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_user_data);
        ButterKnife.bind(this);
        setupActionbar(getString(R.string.lbl_adduser), true);
        setdatecurrent();
        setspinarAdapter();
        checkmalefemalehobby();
        getDataForUpdate();
    }

    void getDataForUpdate()
    {
        if(getIntent().hasExtra(Const.USER_OBJECT)){
            userModel = (UserModel) getIntent().getSerializableExtra(Const.USER_OBJECT);

            etname.setText(userModel.getName());
            etfname.setText(userModel.getFatherName());
            etsname.setText(userModel.getSurName());
            etphone.setText(userModel.getPhoneNumber());
            etdob.setText(userModel.getDob());
            etemail.setText(userModel.getEmail());
            if(userModel.getGender()==Const.male)
            {
                rbmale.setChecked(true);
            }
            else {
                rgfemale.setChecked(true);
            }
            splanguage.setSelection(getSelectedPositionLanguangeId(userModel.getLanguangeId()));
            spcity.setSelection(getSelectedPositionCityId(userModel.getCityId()));


        }
    }


    int getSelectedPositionCityId(int cityId)
    {
        for(int i=0;i<citylist.size();i++)
        {
            if (citylist.get(i).getCityId()==cityId)
            {
                return i;
            }
        }
        return 0;
    }
    int getSelectedPositionLanguangeId(int languangeId)
    {
        for(int i=0;i<languangelist.size();i++)
        {
            if (languangelist.get(i).getLanguangeId()==languangeId)
            {
                return i;
            }
        }

        return 0;
    }
    void setspinarAdapter() {
        citylist.addAll(new TblmstCity(this).getCityList());
        languangelist.addAll(new Tblmstlanguange(this).getLanguangesList());

        cityAdapter = new CityAdapter(this, citylist);
        languageAdapter = new LanguageAdapter(this, languangelist);

        spcity.setAdapter(cityAdapter);
        splanguage.setAdapter(languageAdapter);

    }


    void checkmalefemalehobby() {
        rggender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.rbmale) {
                    chkcrik.setVisibility(View.VISIBLE);
                    chkhoky.setVisibility(View.VISIBLE);
                    chkfootbol.setVisibility(View.VISIBLE);
                } else {
                    chkcrik.setVisibility(View.VISIBLE);
                    chkhoky.setVisibility(View.VISIBLE);
                    chkfootbol.setVisibility(View.GONE);
                }
            }
        });
    }

    void setdatecurrent() {
        final Calendar newCalendar = Calendar.getInstance();
        etdob.setText(String.format("%02d", newCalendar.get(Calendar.DAY_OF_MONTH)) + "/" +
                String.format("%02d", newCalendar.get(Calendar.MONTH)) + "/" +
                String.format("%02d", newCalendar.get(Calendar.YEAR)));
    }

    @OnClick(R.id.etdob)
    public void onEtdobClicked() {
        final Calendar newCalendar = Calendar.getInstance();
        DatePickerDialog datePickerDialog = new DatePickerDialog(
                AdduserActivity.this, (datePicker, year, month, day) -> {
            etdob.setText(day + "/" + (month + 1) + "/" + year);
        },
                newCalendar.get(Calendar.YEAR),
                newCalendar.get(Calendar.MONTH),
                newCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.show();
    }

    @OnClick(R.id.btnsubmit)
    public void onBtnsubmitClicked()
    {

        if (isvaliduser())
        {
            if (userModel == null)
            {
                String hobbies = "";
                if (chkcrik.isChecked()) {
                    hobbies += "," + chkcrik.getText().toString();
                }
                if (chkhoky.isChecked()) {
                    hobbies += "," + chkhoky.getText().toString();
                }
                if (chkfootbol.isChecked()) {
                    hobbies += "," + chkfootbol.getText().toString();
                }
                if (hobbies.length() > 0) {
                    hobbies.substring(1);
                }
                Long lastid = new TblUser(getApplicationContext()).insertuser(etname.getText().toString(),
                        etfname.getText().toString(), etsname.getText().toString(),
                        rbmale.isChecked() ? Const.male : Const.female, hobbies, Utils.getformatedstingtoinsert(etdob.getText().toString()),
                        etphone.getText().toString(), languangelist.get(splanguage.getSelectedItemPosition()).getLanguangeId(),
                        citylist.get(spcity.getSelectedItemPosition()).getCityId(), etemail.getText().toString(),0);

                if (lastid > 0) {
                    Toast.makeText(getApplicationContext(), "user inserted sussfully", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "user not inserted sussfully", Toast.LENGTH_SHORT).show();
                }
            }
            else
                {
                    String hobbies = "";
                    if (chkcrik.isChecked()) {
                        hobbies += "," + chkcrik.getText().toString();
                    }
                    if (chkhoky.isChecked()) {
                        hobbies += "," + chkhoky.getText().toString();
                    }
                    if (chkfootbol.isChecked()) {
                        hobbies += "," + chkfootbol.getText().toString();
                    }
                    if (hobbies.length() > 0) {
                        hobbies.substring(1);
                    }
                    int lastid = new TblUser(getApplicationContext()).udateuser(etname.getText().toString(),
                            etfname.getText().toString(), etsname.getText().toString(),
                            rbmale.isChecked() ? Const.male : Const.female, hobbies, Utils.getformatedstingtoinsert(etdob.getText().toString()),
                            etphone.getText().toString(), languangelist.get(splanguage.getSelectedItemPosition()).getLanguangeId(),
                            citylist.get(spcity.getSelectedItemPosition()).getCityId(), etemail.getText().toString(),userModel.getIsFavorite(),userModel.getUserId());

                    if (lastid > 0) {
                        Toast.makeText(getApplicationContext(), "user Update sussfully", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "user not Update sussfully", Toast.LENGTH_SHORT).show();
                    }

                }
            Intent intent = new Intent(AdduserActivity.this,ActivityUserListGender.class);
            startActivity(intent);
        }

    }

    Boolean isvaliduser() {
        boolean isvalid = true;
        if (TextUtils.isEmpty(etname.getText().toString())) {
            isvalid = false;
            etname.setError("Enetr valid Name");
        }
        if (TextUtils.isEmpty(etfname.getText().toString())) {
            isvalid = false;
            etfname.setError("Enetr father Name");
        }
        if (TextUtils.isEmpty(etsname.getText().toString())) {
            isvalid = false;
            etsname.setError("Enetr sur Name");
        }
        if (TextUtils.isEmpty(etphone.getText().toString())) {
            isvalid = false;
            etphone.setError("Enetr Phone Number");
        } else if (etphone.getText().toString().length() < 10) {
            isvalid = false;
            etphone.setError("enter valid number");
        }
        if (TextUtils.isEmpty(etemail.getText().toString())) {
            isvalid = false;
            etemail.setError("Enetr Email addrs");
        } else {
            String emil = etemail.getText().toString().trim();
            String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
            if (!emil.matches(emailPattern)) {
                etemail.setError("email is not valid");
                isvalid = false;
            }
        }
        if (spcity.getSelectedItemPosition() == 0) {
            isvalid = false;
            Showtost("select City");
        }
        if (splanguage.getSelectedItemPosition() == 0) {
            isvalid = false;
            Showtost("Select Languange");
        }
        return isvalid;
    }

}
