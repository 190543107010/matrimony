package com.example.matrimony.Activitys;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import com.example.matrimony.R;
import com.example.matrimony.adapter.ViewPagerAdapter;
import com.google.android.material.tabs.TabLayout;
import static androidx.fragment.app.FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityUserListGender extends baseactivity {
    @BindView(R.id.tlGenders)
    TabLayout tlGenders;
    @BindView(R.id.vpUserList)
    ViewPager vpUserList;

    ViewPagerAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gender_wise_list);
        ButterKnife.bind(this);
        setupActionbar(getString(R.string.lbl_Userlist),true);
        setupViewPagerAdapter();
    }

    void setupViewPagerAdapter()
    {
        adapter = new ViewPagerAdapter(getSupportFragmentManager(),BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT, this);
        vpUserList.setAdapter(adapter);
        tlGenders.setupWithViewPager(vpUserList);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_favorite,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==R.id.ivFavoriteUser){
            Intent intent = new Intent(ActivityUserListGender.this, UserListActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ActivityUserListGender.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

}
