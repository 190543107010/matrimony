package com.example.matrimony.Activitys;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.matrimony.Model.UserModel;
import com.example.matrimony.R;
import com.example.matrimony.adapter.UserListAdapter;
import com.example.matrimony.database.TblUser;
import com.example.matrimony.util.Const;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ActivitySearchUser extends baseactivity {

    @BindView(R.id.etUserSerch)
    EditText etUserSerch;
    @BindView(R.id.ivserch)
    ImageView ivserch;
    @BindView(R.id.rcvUsers)
    RecyclerView rcvUsers;

    ArrayList<UserModel> userlist = new ArrayList<>();
    ArrayList<UserModel> Tempuserlist = new ArrayList<>();
    UserListAdapter adapter;
    @BindView(R.id.tvnodatafound)
    TextView tvnodatafound;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_user);
        ButterKnife.bind(this);
        setupActionbar(getString(R.string.search_user), true);
        setAdapter();
        setSearchUser();
    }

    void ResetAdapter() {
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }

    void setSearchUser() {
        etUserSerch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                Tempuserlist.clear();
                if (charSequence.toString().length() > 0) {

                    for (int j = 0; j < userlist.size(); j++) {
                        if (userlist.get(j).getName().toLowerCase().contains(charSequence.toString().toLowerCase())
                                || userlist.get(j).getFatherName().toLowerCase().contains(charSequence.toString().toLowerCase())
                                || userlist.get(j).getSurName().toLowerCase().contains(charSequence.toString().toLowerCase())
                                || userlist.get(j).getEmail().toLowerCase().contains(charSequence.toString().toLowerCase())
                                || userlist.get(j).getPhoneNumber().toLowerCase().contains(charSequence.toString().toLowerCase())
                        ) {
                            Tempuserlist.add(userlist.get(j));
                        }
                    }
                }
                if (charSequence.toString().length() == 0 && Tempuserlist.size() == 0) {
                    Tempuserlist.addAll(userlist);
                }

                ResetAdapter();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    void showAlertDiloag(int position) {
        AlertDialog alertDialog = new AlertDialog.Builder(getApplication()).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("Are you sure you want to delete this user");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "YES",
                (dialog, which) -> {
                    int deletedUserbyId = new TblUser(getApplication()).deleteuser(userlist.get(position).getUserId());
                    if (deletedUserbyId > 0) {
                        Toast.makeText(getApplication(), "Deleted successfully", Toast.LENGTH_SHORT).show();
                        userlist.remove(position);
                        adapter.notifyItemRemoved(position);
                        adapter.notifyItemRangeChanged(0, userlist.size());
                        checkandvisible();
                    } else {
                        Toast.makeText(getApplication(), "Somthinf went wrong", Toast.LENGTH_SHORT).show();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "NO",
                (dialog, which) -> dialog.dismiss());
        alertDialog.show();
    }

    void setAdapter() {
        rcvUsers.setLayoutManager(new GridLayoutManager(this, 1));
        userlist.addAll(new TblUser(this).getUsersList());
        Tempuserlist.addAll(userlist);
        adapter = new UserListAdapter(this, Tempuserlist, new UserListAdapter.onViewCilickListner() {
            @Override
            public void onclick(int position) {
                showAlertDiloag(position);
            }

            @Override
            public void onFavroiteClick(int position) {
                int lastUpdatedUserId = new TblUser(getApplication()).updateFavoriteStatus(userlist.get(position).getIsFavorite() == 0 ? 1 : 0, userlist.get(position).getUserId());
                if (lastUpdatedUserId > 0) {
                    userlist.get(position).setIsFavorite(userlist.get(position).getIsFavorite() == 0 ? 1 : 0);
                    adapter.notifyItemChanged(position);
                }
            }

            @Override
            public void onItemclick(int position) {
                Intent intent = new Intent(getApplicationContext(), AdduserActivity.class);
                intent.putExtra(Const.USER_OBJECT, userlist.get(position));
                startActivity(intent);
            }
        });
        rcvUsers.setAdapter(adapter);
    }

    void checkandvisible() {
        if (userlist.size() > 0) {
            tvnodatafound.setVisibility(View.GONE);
            rcvUsers.setVisibility(View.VISIBLE);
        } else {
            tvnodatafound.setVisibility(View.VISIBLE);
            rcvUsers.setVisibility(View.GONE);
        }
    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ActivitySearchUser.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @OnClick(R.id.ivserch)
    public void onViewClicked() {
    }
}
