package com.example.matrimony.Model;

import java.io.Serializable;

public class UserModel implements Serializable {
    int UserId;
    String Name;
    String FatherName;
    int Gender;
    String SurName;
    String Hobby;
    String Dob;
    String PhoneNumber;
    int LanguangeId;
    int CityId;
    String Languange;
    String City;
    String Email;
    int isFavorite;

    public int getIsFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(int isFavorite) {
        this.isFavorite = isFavorite;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getLanguange() {
        return Languange;
    }

    public void setLanguange(String languange) {
        Languange = languange;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getFatherName() {
        return FatherName;
    }

    public void setFatherName(String fatherName) {
        FatherName = fatherName;
    }

    public int getGender() {
        return Gender;
    }

    public void setGender(int gender) {
        Gender = gender;
    }

    public String getSurName() {
        return SurName;
    }

    public void setSurName(String surName) {
        SurName = surName;
    }

    public String getHobby() {
        return Hobby;
    }

    public void setHobby(String hobby) {
        Hobby = hobby;
    }

    public String getDob() {
        return Dob;
    }

    public void setDob(String dob) {
        Dob = dob;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public int getLanguangeId() {
        return LanguangeId;
    }

    public void setLanguangeId(int languangeId) {
        LanguangeId = languangeId;
    }

    public int getCityId() {
        return CityId;
    }

    public void setCityId(int cityId) {
        CityId = cityId;
    }

    @Override
    public String toString() {
        return "UserModel{" +
                "UserId=" + UserId +
                ", Name='" + Name + '\'' +
                ", FatherName='" + FatherName + '\'' +
                ", Gender=" + Gender +
                ", SurName='" + SurName + '\'' +
                ", Hobby='" + Hobby + '\'' +
                ", Dob='" + Dob + '\'' +
                ", PhoneNumber='" + PhoneNumber + '\'' +
                ", LanguangeId=" + LanguangeId +
                ", CityId=" + CityId +
                ", Languange='" + Languange + '\'' +
                ", City='" + City + '\'' +
                ", Email='" + Email + '\'' +
                ", isFavorite=" + isFavorite +
                '}';
    }
}
