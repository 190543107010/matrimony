package com.example.matrimony.Model;

import java.io.Serializable;

public class LanguangeModel implements Serializable {
    int LanguangeId;
    String Name;

    public int getLanguangeId() {
        return LanguangeId;
    }

    public void setLanguangeId(int languangeId) {
        LanguangeId = languangeId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    @Override
    public String toString() {
        return "LanguangeModel{" +
                "LanguangeId=" + LanguangeId +
                ", Name='" + Name + '\'' +
                '}';
    }
}
