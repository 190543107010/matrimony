package com.example.matrimony.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.matrimony.Activitys.AdduserActivity;
import com.example.matrimony.Model.UserModel;
import com.example.matrimony.R;
import com.example.matrimony.adapter.UserListAdapter;
import com.example.matrimony.database.TblUser;
import com.example.matrimony.util.Const;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UserListFragment extends Fragment {

    @BindView(R.id.rcvUserlist)
    RecyclerView rcvUserlist;

    ArrayList<UserModel> userlist = new ArrayList<>();
    UserListAdapter adapter;
    @BindView(R.id.tvnodatafound)
    TextView tvnodatafound;
    @BindView(R.id.btnAdduser)
    FloatingActionButton btnAdduser;

    public static UserListFragment getInstance(int gender) {
        UserListFragment fragment = new UserListFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(Const.gender, gender);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_user_list, null);
        setHasOptionsMenu(true);
        ButterKnife.bind(this, v);
        setAdapter();
        return v;
    }


    void openAddUserScreen()
    {
        Intent integer = new Intent(getActivity(), AdduserActivity.class);
        startActivity(integer);
    }


    void showAlertDiloag(int position) {
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("Are you sure you want to delete this user");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "YES",
                (dialog, which) -> {
                    int deletedUserbyId = new TblUser(getActivity()).deleteuser(userlist.get(position).getUserId());
                    if (deletedUserbyId > 0) {
                        Toast.makeText(getActivity(), "Deleted successfully", Toast.LENGTH_SHORT).show();
                        userlist.remove(position);
                        adapter.notifyItemRemoved(position);
                        adapter.notifyItemRangeChanged(0, userlist.size());
                        checkandvisible();
                    } else {
                        Toast.makeText(getActivity(), "Somthinf went wrong", Toast.LENGTH_SHORT).show();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "NO",
                (dialog, which) -> dialog.dismiss());
        alertDialog.show();
    }

    void setAdapter() {
        rcvUserlist.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        userlist.addAll(new TblUser(getActivity()).getuserbygender(getArguments().getInt(Const.gender)));
        adapter = new UserListAdapter(getActivity(), userlist, new UserListAdapter.onViewCilickListner() {
            @Override
            public void onclick(int position) {
                showAlertDiloag(position);
            }

            @Override
            public void onFavroiteClick(int position) {
                int lastUpdatedUserId = new TblUser(getActivity()).updateFavoriteStatus(userlist.get(position).getIsFavorite() == 0 ? 1 : 0,userlist.get(position).getUserId());
                if(lastUpdatedUserId > 0){
                    userlist.get(position).setIsFavorite(userlist.get(position).getIsFavorite() == 0?1:0);
                    adapter.notifyItemChanged(position);
                }
            }

            @Override
            public void onItemclick(int position) {
                Intent intent = new Intent(getActivity(),AdduserActivity.class);
                intent.putExtra(Const.USER_OBJECT,userlist.get(position));
                startActivity(intent);
            }
        });
        rcvUserlist.setAdapter(adapter);
        checkandvisible();
    }

    void checkandvisible() {
        if (userlist.size() > 0) {
            tvnodatafound.setVisibility(View.GONE);
            rcvUserlist.setVisibility(View.VISIBLE);
        } else {
            tvnodatafound.setVisibility(View.VISIBLE);
            rcvUserlist.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.btnAdduser)
    public void onViewClicked() {
        openAddUserScreen();
    }
}
